var abaSelecionada = '';

// Função para abrir as abas da sessão serviços
// Esta função adiciona a classe 'active' à aba, verifica este atributo e preve que ela feche caso o possua

$(".abas-container").bind("click", function(e) {
    if(abaSelecionada == $(this).attr("data-ref") ) return false;
    e.preventDefault();
    $(".aba-conteudo").slideUp();
    $( $(this).attr("data-ref") ).slideDown();
    $(".abas-container").removeClass("active");
    $(this).addClass("active");
    abaSelecionada = $(this).attr("data-ref");
    return false;
});

// Ao iniciar, após a instrução acima, clica na primeira aba, e abre o primeiro slide.
$(".abas-container").first().click();

// Função que atribui o valor .check para a caixa de diálogo da sessão Orçamento
$("span.checkbox-input").bind("mouseup", function(e) {
	var _check = $(this).children(".check");
	_check.toggleClass("hidden");
	var input = $(this).children("input[type='checkbox']");
	input.click();
});

// Slide do menu versão Mobile que se adapta conforme o tamanho da tela
$(".barras").bind("click", function() {
    $(".mobile-expandido").slideToggle();
});

// Animações do menu-fixo que rola com a página
$(window).bind("scroll", function() {
    var scrollY = window.scrollY;

    if( scrollY > $(".menu-principal").height() ) {
        $(".menu-fixo").slideDown();
    } else {
        $(".menu-fixo").slideUp();
    }
});

// Animação para rolar sessões na barra de navegação.
$(document).on('click', 'a', function(event){
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
});
